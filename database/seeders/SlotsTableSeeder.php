<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SlotsTableSeeder extends Seeder
{
    private $data;

    public function __construct(Type $var = null)
    {
        $this->data = [
            [
                'slot_name' => 'BAMBOO RUSH',
                'slot_url' => 'https://latamwin-gp3.discreetgaming.com/cwguestlogin.do?bankId=3006&gameId=806&lang=es',
                'slot_image_url' => 'https://winchiletragamonedas.com/public/images/games/bamboo_rush.jpeg',
                'slot_status' => '1',
            ],
            [
                'slot_name' => 'REELS OF WEALTH',
                'slot_url' => 'https://latamwin-gp3.discreetgaming.com/cwguestlogin.do?bankId=3006&gameId=795&lang=es',
                'slot_image_url' => 'https://winchiletragamonedas.com/public/images/games/reels_of_wealth.jpeg',
                'slot_status' => '1',
            ],
            [
                'slot_name' => 'DRAGON & PHOENIX',
                'slot_url' => 'https://latamwin-gp3.discreetgaming.com/cwguestlogin.do?bankId=3006&gameId=814&lang=es',
                'slot_image_url' => 'https://winchiletragamonedas.com/public/images/games/dragon_phoenix.jpeg',
                'slot_status' => '1',
            ],
            [
                'slot_name' => 'TAKE THE BANK',
                'slot_url' => 'https://latamwin-gp3.discreetgaming.com/cwguestlogin.do?bankId=3006&gameId=813&lang=es',
                'slot_image_url' => 'https://winchiletragamonedas.com/public/images/games/take_the_bank.jpeg',
                'slot_status' => '1',
            ],
            [
                'slot_name' => 'CAISHEN’S ARRIVAL',
                'slot_url' => 'https://latamwin-gp3.discreetgaming.com/cwguestlogin.do?bankId=3006&gameId=812&lang=es',
                'slot_image_url' => 'https://winchiletragamonedas.com/public/images/games/caishens_arrival.jpeg',
                'slot_status' => '1',
            ],
            [
                'slot_name' => 'GEMMED!',
                'slot_url' => 'https://latamwin-gp3.discreetgaming.com/cwguestlogin.do?bankId=3006&gameId=811&lang=es',
                'slot_image_url' => 'https://winchiletragamonedas.com/public/images/games/gemmed.jpeg',
                'slot_status' => '1',
            ],
        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slots')->insert($this->data);
    }
}
