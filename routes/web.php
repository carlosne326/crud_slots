<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SlotController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SlotController::class, 'index']);
Route::get('/new', [SlotController::class, 'create']);
Route::post('/insert', [SlotController::class, 'store']);
Route::get('/edit/{slot}', [SlotController::class, 'edit']);
Route::post('/update/{slot}', [SlotController::class, 'update']);
Route::post('/delete/{slot}', [SlotController::class, 'delete']);
