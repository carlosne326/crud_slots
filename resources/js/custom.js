// tooltip init
import {Tooltip} from 'bootstrap';

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle1="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new Tooltip(tooltipTriggerEl)
})

// confirmar borrado
var delete_post_action = document.getElementsByClassName("delete-slot");

var deleteAction = function(e) {
  if(!confirm('Estas seguro que quieres eliminar este Slot?')) {
    e.preventDefault();
  }
  return false;
}

for (var i = 0; i < delete_post_action.length; i++) {
    delete_post_action[i].addEventListener('click', deleteAction, false);
}