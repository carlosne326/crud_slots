@extends('layout')
     
@section('content')

<div class="row flex-lg-nowrap">

    <div class="col">
        <div class="e-tabs mb-3 px-3">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" href="#">Slots</a></li>
            </ul>
        </div>
         @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <div class="row flex-lg-nowrap">
            <div class="col mb-3">
                <div class="e-panel card">
                    <div class="card-body">
                        <div class="e-table">
                            <div class="table-responsive table-lg mt-3">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Imagen</th>
                                            <th>Nombre</th>
                                            <th>Estado</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($slots as $slot)
                                            <tr>
                                                <td class="align-middle text-center">
                                                    <div class="bg-light d-inline-flex justify-content-center align-items-center align-top">
                                                        <img src="{{ $slot->slot_image_url }}" class="slot-img">
                                                    </div>
                                                </td>
                                                <td class="text-nowrap align-middle">{{ $slot->slot_name }}</td>
                                                <td class="text-nowrap align-middle">{{ $slot->slot_status == 1 ? 'Activo' : 'Inactivo' }}</td>
                                                <td class="text-center align-middle">
                                                    <div class="btn-group align-top">
                                                        <div>
                                                            <a 
                                                                class="btn btn-success" 
                                                                data-bs-toggle1="tooltip" 
                                                                data-bs-placement="top"
                                                                title="Ejecutar"
                                                                href="{{ $slot->slot_url }}"
                                                                target="_blank"
                                                            >
                                                                <i class="fas fa-play"></i>
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <a 
                                                                class="btn btn-primary"
                                                                data-bs-toggle1="tooltip"
                                                                data-bs-placement="top"
                                                                title="Editar"
                                                                href="edit/{{ $slot->id }}"
                                                            >
                                                                <i class="fas fa-edit"></i>
                                                            </a>
                                                        </div>
                                                        <form method="post" action="delete/{{ $slot->id }}">
                                                        @csrf
                                                            <button type="submit" class="btn btn-danger delete-slot" data-bs-toggle1="tooltip" data-bs-placement="top" title="Eliminar">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-3 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center px-xl-3">
                            <a class="btn btn-success btn-block" href="/new">Agregar Slot</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Eliminar</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
