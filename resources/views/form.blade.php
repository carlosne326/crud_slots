@extends('layout')
     
@section('content')

<div class="row flex-lg-nowrap">
    <div class="col">
        <div class="e-tabs mb-3 px-3">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" href="#">{{ $title }}</a></li>
            </ul>
        </div>

        <div class="row flex-lg-nowrap">
            <div class="col mb-3">
                <div class="e-panel card">
                    <div class="card-body">
                        <form class="form" method="POST" enctype="multipart/form-data" action="{{ $action }}">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <div class="row mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input 
                                                    class="form-control"
                                                    name="name"
                                                    type="text"
                                                    placeholder="Gates of Olympus"
                                                    @isset ($slot)
                                                        value="{{ $slot->slot_name }}"
                                                    @endisset
                                                >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Url</label>
                                                <input 
                                                    class="form-control"
                                                    name="url"
                                                    type="text"
                                                    placeholder="https://latamwin-gp3.discreetgaming.com/cwguestlogin.do?bankId=3006&gameId=795&lang=es"
                                                    @isset ($slot)
                                                        value="{{ $slot->slot_url }}"
                                                    @endisset
                                                >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Imagen</label>
                                                <input type="file" name="image" class="form-control" placeholder="image" accept="image/*">
                                                @isset ($slot)
                                                    @if (str_starts_with($slot->slot_image_url, 'http'))
                                                        <img src="{{ $slot->slot_image_url }}" class="slot-img">
                                                    @else    
                                                        <img src="{{ url("/") . "/" . $slot->slot_image_url }}" class="slot-img">
                                                    @endif
                                                @endisset
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label>Estado</label>
                                                <select class="form-select" name="status">
                                                        <option value="1" >Activo</option>
                                                    @if (isset($slot))
                                                        <option value="0" {{ $slot->slot_status == 0 ? 'selected' : '' }}>Inactivo</option>
                                                    @else
                                                        <option value="0">Inactivo</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col d-flex justify-content-end">
                                    <button class="btn btn-primary" type="submit">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection