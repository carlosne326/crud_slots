# crud_slots

Crud de slots

## Requerimientos

* Composer
* Node

## Iniciando el proyecto

Una vez clonado el repositorio y configurado el acceso a una base de datos, se deben ejecutar los siguientes comandos:

```bash
$ composer install
$ php artisan migrate
$ php artisan db:seed --class=SlotsTableSeeder
$ npm install
$ npx mix
```

