<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slot;

class SlotController extends Controller
{
    public function index()
    {
        $slots = Slot::all();
        return view('list', [
            'slots' => $slots
        ]);
    }

    public function create()
    {
        return view('form', [
            'title' => 'Nuevo Slot',
            'action' => '/insert'
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'status' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $slot = new Slot;

        $slot->slot_name = $request->input('name');
        $slot->slot_url = $request->input('url');
        $slot->slot_status = $request->input('status');
  
        if ($image = $request->file('image')) {
            $destination_path = 'image/';
            $filename = str_replace(' ', '_', $slot->name) . "_" . date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destination_path, $filename);
            $slot->slot_image_url = "image/$filename";
        }
    
        $slot->save();
     
        return redirect('/')
            ->with('success','Slot Añadido!');
    }

    public function edit(Slot $slot)
    {
        return view('form', [
            'title' => 'Editar Slot',
            'action' => "/update/$slot->id",
            'slot' => $slot
        ]);
    }

    public function update(Request $request, Slot $slot)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'status' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
  
        $slot->slot_name = $request->input('name');
        $slot->slot_url = $request->input('url');
        $slot->slot_status = $request->input('status');
  
        if ($image = $request->file('image')) {
            $destination_path = 'image/';
            $filename = str_replace(' ', '_', $slot->name) . "_" . date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destination_path, $filename);
            $slot->slot_image_url = "image/$filename";
        }
          
        $slot->save();
    
        return redirect('/')
            ->with('success','Slot modificado!');
    }

    public function delete(Slot $slot)
    {
        $slot->delete();
        return redirect('/')
            ->with('success','Slot eliminado!');
    }
}
